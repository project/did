# D-iD
## What is the D-iD module?
Its just a core module that lets you talk to D-iD service. Use the AI
Interpolator plugin to have any use of it.

### How to install the module
1. Get the code like any other module.
2. Install the module.
3. Setup the Did API key here: /admin/config/did/settings
