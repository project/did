<?php

namespace Drupal\ai_interpolator_did\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Token;
use Drupal\did\Did;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The rules for a video field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_did_video",
 *   title = @Translation("D-iD Avatar from Audio"),
 *   field_rule = "file",
 *   target = "file",
 * )
 */
class AudioToDidAvatar extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'D-iD Avatar from Audio';

  /**
   * The entity type manager.
   */
  public EntityTypeManagerInterface $entityManager;

  /**
   * The Did requester.
   */
  public Did $did;

  /**
   * The File System interface.
   */
  public FileSystemInterface $fileSystem;

  /**
   * The File Repo.
   */
  public FileRepositoryInterface $fileRepo;

  /**
   * The token system to replace and generate paths.
   */
  public Token $token;

  /**
   * The current user.
   */
  public AccountProxyInterface $currentUser;

  /**
   * The logger channel.
   */
  public LoggerChannelFactoryInterface $loggerChannel;

  /**
   * Construct an image field.
   *
   * @param array $configuration
   *   Inherited configuration.
   * @param string $plugin_id
   *   Inherited plugin id.
   * @param mixed $plugin_definition
   *   Inherited plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   The entity type manager.
   * @param \Drupal\did\Did $did
   *   The Did requester.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The File system interface.
   * @param \Drupal\file\FileRepositoryInterface $fileRepo
   *   The File repo.
   * @param \Drupal\Core\Utility\Token $token
   *   The token system.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannel
   *   The logger channel interface.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityManager,
    Did $did,
    FileSystemInterface $fileSystem,
    FileRepositoryInterface $fileRepo,
    Token $token,
    AccountProxyInterface $currentUser,
    LoggerChannelFactoryInterface $loggerChannel,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entityManager;
    $this->did = $did;
    $this->fileSystem = $fileSystem;
    $this->fileRepo = $fileRepo;
    $this->token = $token;
    $this->currentUser = $currentUser;
    $this->loggerChannel = $loggerChannel;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('did.api'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('token'),
      $container->get('current_user'),
      $container->get('logger.factory'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function helpText() {
    return $this->t("Generate avatar videos based on audio.");
  }

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function allowedInputs() {
    return [
      'file',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function ruleIsAllowed(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function extraFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form ['did'] = [
      '#type' => 'details',
      '#title' => $this->t('D-iD Settings'),
      '#open' => TRUE,
      '#weight' => 20,
    ];

    $form['did']['interpolator_did_expression'] = [
      '#type' => 'select',
      '#title' => $this->t('Expression'),
      '#description' => $this->t('Choose the expression to use.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_did_expression', ''),
      '#options' => [
        'neutral' => $this->t('Neutral'),
        'happy' => $this->t('Happy'),
        'surprise' => $this->t('Surprise'),
        'serious' => $this->t('Serious'),
      ],
    ];

    $form['did']['interpolator_did_avatar_option'] = [
      '#type' => 'select',
      '#title' => $this->t('Avatar type'),
      '#description' => $this->t('Choose the image to generate from. Uploading config is not exported/imported, so we do not allow that.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_did_avatar_option', ''),
      '#options' => [
        'url' => $this->t('Image Url'),
        'field' => $this->t('Image field'),
      ],
    ];

    $form['did']['interpolator_did_file_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image Url'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_did_file_url', ''),
      '#description' => $this->t('This can be a public url online like https://www.example.com/test.jpg or internal file like public://test.jpg.'),
      '#states' => [
        'visible' => [
          ':input[name="interpolator_did_avatar_option"]' => ['value' => 'url'],
        ],
      ],
    ];

    $fields = $this->getFieldsOfType($entity, 'image', 'file');
    $form['did']['interpolator_did_file_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Image Field'),
      '#description' => $this->t('Choose the image field to use.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_did_file_field', ''),
      '#options' => $fields,
      '#states' => [
        'visible' => [
          ':input[name="interpolator_did_avatar_option"]' => ['value' => 'field'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigValues($form, FormStateInterface $formState) {
    $values = $formState->getValues();
    if ($values['interpolator_did_avatar_option'] == 'url' && empty($values['interpolator_did_file_url'])) {
      $formState->setErrorByName('interpolator_did_file_url', $this->t('You need to provide a url.'));
    }
    if ($values['interpolator_did_avatar_option'] == 'field' && empty($values['interpolator_did_file_field'])) {
      $formState->setErrorByName('interpolator_did_file_field', $this->t('You need to provide a field.'));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = [];
    $imageUrl = $interpolatorConfig['did_avatar_option'] == 'url' ? $interpolatorConfig['did_file_url'] : $entity->get($interpolatorConfig['did_file_field'])->entity->getFileUri();
    $audioUrl = $entity->get($interpolatorConfig['base_field'])->entity->getFileUri();
    $result = $this->did->generateVideoFromAudioAndImageSync($audioUrl, $imageUrl, $interpolatorConfig['did_expression']);
    if (!empty($result['result_url'])) {
      $values[] = $result['result_url'];
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Check so the value is a valid url.
    if (filter_var($value, FILTER_VALIDATE_URL)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    // Transform string to boolean.
    $fileEntities = [];

    // Successful counter, to only download as many as max.
    $successFul = 0;
    foreach ($values as $value) {
      // Get better file name
      $fileName = explode('?', basename($value))[0];
      // Everything validated, then we prepare the file path to save to.
      $filePath = $this->token->replace($config['uri_scheme'] . '://' . rtrim($config['file_directory'], '/')) . '/' . $fileName;
      // Create file entity from string.
      $file = $this->generateFileFromUrl($value, $filePath);
      // If we can save, we attach it.
      if ($file) {
        // Add to the entities list.
        $fileEntities[] = [
          'target_id' => $file->id(),
        ];

        $successFul++;
        // If we have enough images, give up.
        if ($successFul == $fieldDefinition->getFieldStorageDefinition()->getCardinality()) {
          break;
        }
      }
    }

    // Then set the value.
    $entity->set($fieldDefinition->getName(), $fileEntities);
  }

  /**
   * Generate a file entity.
   *
   * @param string $url
   *   The url to generate from.
   * @param string $dest
   *   The destination.
   *
   * @return \Drupal\file\FileInterface|false
   *   The file or false on failure.
   */
  private function generateFileFromUrl(string $url, string $dest) {
    // Get binary.
    $binary = file_get_contents($url);
    $path = substr($dest, 0, -(strlen($dest) + 1));
    // Create directory if not existsing.
    $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
    $file = $this->fileRepo->writeData($binary, $dest, FileSystemInterface::EXISTS_RENAME);
    if ($file) {
      return $file;
    }
    return FALSE;
  }

}
